# DnD Utilities

Things to use with DnD to make the table cleaner and tracking resources easier. These 3D models are designed to be simplistic so they can be 3D printed easily.

These things are created in FreeCAD and are completely open-source. You can use and edit them as you wish. If you have and idea what could be created next, you can let me know.

All previewes are printed using Prusa MK3S 3D Printer with 0.6mm nozzle unless stated otherwise.
I usually use 0.4mm layers for these simple things since they are not detailed.

All photos are shot on a potato so excuse the poor quality please.

## Dwarven Spellbook
Covers for spellcards made for dwarf wizard Orsik Gridimdeft.

Designed for A6 format spellcards with punch holes with 8cm pitch. The final spellcards are not precisely A6 but are cropped to 10.5x13.5cm.

_The font used in Dwarven Spellbook is "Viking Elder Runes" downloaded from https://www.dafont.com/viking-elder-runes.font._

![Dwarven Spellbook](Previews/dwarven_spellbook.png)

## Arcane Recovery Counter
Token counter for Arcane Recovery points for wizards.

![Arcane Recovery Counter](Previews/arcane_recovery.png)
